package com.heaven.sky.web.socket.controller;

import com.heaven.sky.web.socket.message.Message;
import com.heaven.sky.web.socket.message.OutputMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ChatController {
    @MessageMapping("/chat")
    @SendTo("/toxic/messages")
    public OutputMessage send(final Message message) throws Exception {
        final String time = new SimpleDateFormat("HH:mm").format(new Date());
        //trả về message cho Client
        // có thể trong project - là 1 xử lý nghiệp vụ nào đó và trả về cho client
        return new OutputMessage(message.getFrom(), message.getText(), time);
    }
}
