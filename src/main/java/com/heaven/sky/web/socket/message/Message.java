package com.heaven.sky.web.socket.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String from;
    private String text;
}
