package com.heaven.sky.web.socket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
        //thêm tiền tố cho send và subcrible message từ client và server
        config.enableSimpleBroker("/toxic"); // thêm tiền tố cho Send message từ Server đến client
        config.setApplicationDestinationPrefixes("/apps");// thêm tiền tố cho Send message từ Client đến server
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        //tạo các endpoint giống các URI của api
        registry.addEndpoint("/chat");
        registry.addEndpoint("/chat").withSockJS();
    }
}
